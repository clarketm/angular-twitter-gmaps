import {
    Component,
    Input,
    AfterViewChecked,
    OnInit,
    OnDestroy,
} from 'angular2/core';

@Component({
    selector: 'subscription',
    templateUrl: 'src/app/subscription.html'
})
export default class SubscriptionComponent implements OnInit, OnDestroy, AfterViewChecked, AfterViewChecked {
    @Input() search:any;
    @Input() pusher;
    public tweets:Object[];
    private channel;
    private subscribed:boolean = false;
    private className:String;
    private googleMap:any;

    public ngOnInit() {
        this.subscribeToChannel();
        this.tweets = [];
        this.className = this.search.term.replace(' ', '-');
    }

    private subscribeToChannel() {
        this.channel = this.pusher.subscribe(btoa(this.search.term));
        this.channel.bind('new_tweet', (data) => {
            console.log(!!data);
            if (data.tweet.geo && data.tweet.user.profile_image_url) {
                this.newTweet(data);
                let lat = data.tweet.geo.coordinates[0];
                let long = data.tweet.geo.coordinates[1];
                var marker = new google.maps.Marker({
                    position: {lat: lat, long: long},
                    map: this.googleMap,
                    title: 'Hello World!'
                });
                console.debug("data", data.tweet.geo.coordinates);
                console.debug("data", data.tweet.user.profile_image_url);
            }
        });
        this.subscribed = true;
    }

    private newTweet(data:Object) {
        this.tweets.push(data);
    }

    // TODO: bring back when working correctly (see bottom of ngAfterViewChecked)
    // This should fire anytime bindings are modified from AppComponent but it's not
    // Don't have time to debug at moment, but fix if you can :)
    // public ngOnChanges() {
    //   console.log(this.search);
    //   if (!this.search.active && this.subscribed) {
    //     this.ngOnDestroy();
    //   } else if (this.search.active && !this.subscribed) {
    //     this.subscribeToChannel();
    //   }
    // }

    public ngOnDestroy() {
        this.pusher.unsubscribe(btoa(this.search.term));
        this.channel && this.channel.unbind();
        this.subscribed = false;
    }

    public ngAfterViewInit() {
        let mapId = document.querySelector("#map-" + this.className),
            listItem = document.querySelector(".channel-" + this.className);
        if (mapId) {
            this.createGoogleMap(mapId);
        }
        if (listItem) {
            listItem.scrollTop = listItem.scrollHeight;
        }
    }

    public createGoogleMap(mapId) {
        this.googleMap = new google.maps.Map(mapId, {
            center: {lat: -34.397, lng: 150.644},
            zoom: 8
        });
    }

    public ngAfterViewChecked() {
        if (!this.search.active && this.subscribed) {
            this.ngOnDestroy();
        } else if (this.search.active && !this.subscribed) {
            this.subscribeToChannel();
        }

    }
}
